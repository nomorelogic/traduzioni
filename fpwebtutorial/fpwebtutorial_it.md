<style type="text/css">
  th, td {
    align: center;
    vertical-align: middle;
    padding: 5px;
  }
</style>

<center>
<h1>Tutorial di base fpWeb</h1>
</center>

----------------------------------------------------

#### _Note sulla traduzione_

Questo testo è una traduzione realizzata da Basso Marcello (aka nomorelogic).

Il documento originale è stato scritto da Mario Ray Mahardhika (aka leledumbo). 
Quì trovate il [post sul forum](http://forum.lazarus.freepascal.org/index.php/topic,29055.msg182833.html#msg182833) con il quale lo ha reso pubblico e trovate anche le sue indicazioni per poter eventualmente contribuire.

Allo stato attuale il documento riporta le immagini originali in lingua inglese, con un po' di tempo arriverà la traduzione anche della grafica.

----------------------------------------------------

<h1>Indice</h1>

[TOC]


# Introduzione

fpWeb è un framework per lo sviluppo di applicazioni web, distribuito con FPC nella distribuzione di default come parte del pacchetto fcl-web.
fdk fasdljf lahjkflsd
Il framework stesso è stato sviluppato sopra le caratteristiche di fcl-web. Il framework è stato sviluppato pensando al RAD in modo da poter sfruttare il sistema di componentizzazione nell'ambito della produzione di contenuti dinamici. Viene fornito un pacchetto per Lazarus per permettere di utilizzare il framework con il drag e drop, per la gestione delle sessioni e per la produzione dei contenuti.

Questo tutorial cercherà di coprire le funzionalità base di fpWeb, in modo da poterci realizzare una comune applicazione web. Si noti che questo tutorial NON ha intenzione di insegnare il protocollo HTTP, HTML, CSS, JavaScript o la manipolazione dati di un database, in quanto il protocollo e linguaggi usati per i client dovrebbero essere dei prerequisiti per ogni programmatore di applicazioni web e, la manipolazione di database, non differisce da una implementazione desktop.


# Architettura (leggere PER FAVORE)

Prima di iniziare, è necessario conoscere l'architettura ed il flusso applicativo, per evitare confusione quando certe cose non funzioneranno o funzioneranno in modo inaspettato. Quindi, per favore, investite un po' di tempo nel leggere questa sezione.

## Applicazione

Applicazione qui fa riferimento al protocollo che la vostra app implementerà. fpWeb sarà lieto di passare da CGI, FCGI, modulo Apache ad embedded server ed altro ancora, se fclWeb ne implementerà un altro in futuro. Ogni applicazione è implementata una propria unit così, per passare da un'applicazione all'altra, ad eccezione del modulo Apache, uno deve solo cambiare il rispettivo identificatore nella clausola uses. Attualmente (come in 2.6.4 / 3.1.1), sono:

* fpCGI -> CGI
* fpFCGI -> FastCGI
* fpApache (richiede httpd) -> modulo Apache
* fpHttpApp -> embedded server

In questo tutorial, useremo embedded server per semplicità, per non avere a che fare con l'impostazione di un server virtuale e con macchinosi e complicati file di configurazione e gestione di servizi. La vostra applicazione sarà una applicazione web come unico binario portabile! Un altro motivo potrebbe essere che, in giro, c'è più di una applicazione server web ed ognuna ha modo diverso di essere configurata. Sarebbe eccessivo approfondirle tutte, in quanto la loro documentazione fa già questo lavoro.

Il modulo Apache è implementato come una libreria (dinamica) mentre gli altri protocolli sono un normale applicazione. Ogni applicazione può avere proprietà specifiche (come le porte) disponibili e significativi solo per quella applicazione. Ecco perché se si guardano gli esempi fclweb, la coppia di file .lpi / .lpr, per ciascun protocollo, vengono messi nelle proprie directory, solo i moduli web sono in comune.

## Moduli Web

Le applicazioni fpWeb sono costituite da moduli web che si occupano della produzione dei contenuti effettivi. Un modulo Web può contenere azioni, web action, che possono suddividerne le funzionalità in modo più specifico. Ad esempio, un modulo web di autenticazione potrebbe avere le web action login e logout. Mentre un modulo web di about potrebbe non avere bisogno di azioni e fornire un solo contenuto. Il modulo Web è integrato con fpTemplate che può essere utilizzato per la produzione di contenuti dinamici a partire da un file di modello. Questo è più o meno simile a quello che fa il PHP, solo che il divario tra la logica e la presentazione è costretto piuttosto che suggerito. Alcuni dicono che fpTemplate implementa una vista passiva mentre PHP, per impostazione predefinita, implementa un modello design a vista attiva (active view design pattern).

# Installazione

Il package fpWeb per Lazarus non è installato di default (ma viene fornito). Quindi lanciate il vostro Lazarus e scegliete `Package->Configura i pacchetti installati...` (N.d.T.: si aprirà la finestra 'Installa/Disinstalla package'), nella listbox di riepilogo `disponibili per l'installazione`, cercare `weblaz` e premete `Installa selezione`. Premere `Salva e ricostruisci l'IDE` e confermare con `Continua`. Lasciate che l'IDE venga ricostruito e che si riavvii da sé. Se tutto è andato bene, ora si dovrebbe avere il tab fpWeb nella palette dei componenti.

![Package weblaz installato](installweblaz.png "Package weblaz installato")

Package weblaz installato


# Hello, World!

Come comunemente si insegna quando si impara la programmazione, "Hello, World!" sarà la nostra prima applicazione. Aprite Lazarus e scegliete `Progetto->Nuovo Progetto` quindi scegliere "HTTP server
Application".


![Creazione nuova applicazione HTTP server](weblazhttpappcreateproject.png "Creazione nuova applicazione HTTP server")

Creazione nuova applicazione HTTP server



Apparirà un'altra finestra tipo dialog con opzioni per servire file statici, selezione della porta e multithreading. Si può saltare la prima ed eventualmente scegliere la seconda, ma non importa molto in questo tutorial.

![Opzioni per file statici, selezione porta e multithreading](weblazhttpappstaticfiles.png "Opzioni per file statici, selezione porta e multithreading")

Opzioni per file statici, selezione porta e multithreading



**IMPORTANTE!**:

Se si sceglie di utilizzare i thread su *nix, non dimenticare di aggiungere `cthreads` come prima unit nella clausola uses del file di progetto .lpr, altrimenti verrà generato un RTE 232. Quando si eseguirà da console, apparirà il seguente messaggio:

> "This binary has no thread support compiled in.
> Recompile the application with a thread-driver in the program uses clause
> before other units using thread."

N.d.T
> "Questo binario non è stato compilato con il supporto per i thread.
> Ricompilare l'applicazione con un thread-driver nella clausola uses
> prima delle altre unit che usano i thread."

Qualunque cosa selezioniate, cliccate su "OK" e vi verrà presentato il modulo di default della fpWeb app. Selezionate il modulo e spostatevi sull'object inspector. Sentitevi liberi di rinominare il modulo se lo volete. Selezionate il tab Eventi e fare clic sul pulsante sulla destra della seconda colonna della riga OnRequest, per creare il gestore dell'evento.


![Creazione del gestore dell'evento OnRequest del modulo web tramite object inspector](webmoduleonrequestobjectinspector.png "Creazione del gestore dell'evento OnRequest del modulo web tramite object inspector")

Creazione del gestore dell'evento OnRequest del modulo web tramite object inspector



Sarai reindirizzato all'editor dei sorgenti con il seguente codice:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
begin
  |
end;
```

Scrivere nell'evento:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
begin
  AResponse.Content := 'Hello, World!';
  Handled := true;
end;
```

Quindi lanciate la vostra applicazione. Ricordate la porta selezionata in precedenza o semplicemente aprire il file .lpr per vederla. Aprire il browser e digitate: `http://localhost:<your selected port>/`

Dovreste vedere visualizzato "Hello, World!". Se così non fosse, controllare qui sotto:

Il framework solleva molte eccezioni da gestire e il debugger dell'IDE le intercetta ed interrompe l'applicazione. E' OK aggiungere la maggior parte delle eccezioni alla lista di quelle da ignorare in modo da potersi concentrare maggiormente sul flusso dell'applicazione. Saltare e continuare finché non appaiono più dialog ed il browser mostra l'output.

`Handled := true` è il modo di dire al framework che la richiesta è stata gestita.
Non impostarla (o impostanrla a *false*) e verrà invece mostrata la pagina di errore. Per ora, questo non influisce ancora sul flusso della richiesta, ma lo farà più avanti. Potere quindi continuare a fare in questo modo finché arriverà il momento di usarlo a dovere.

# Lettura dati GET e POST

Un contenuto dinamico è probabile che venga attivato sia dall'input di un utente che attraverso delle form,
fornendo valori nell'URL, ecc. Questi dati vengono inviati insieme alla richiesta, che è rappresentata
no metodo dal parametro `ARequest` di tipo `TRequest`.

## Lettura GET

I dati GET sono disponibili in `ARequest.QueryFields`, che è un discendente di `TStrings`. In breve, tutto ciò che si può di solito fare con TStrings, è applicabile anche quì, come per esempio l'accesso ai dati in stile mappa tramite la proprietà `Values`.

Riutilizzando il codice sopra, sostituire il corpo del metodo con:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
var
  LName: String;
begin
  LName := ARequest.QueryFields.Values['Name'];
  if LName = EmptyStr then
    with AResponse.Contents do begin
      Add('<form action="' + ARequest.URI + '" method="GET"');
      Add('<label for="name">Per favore dimmi il tuo nome:</label>');
      Add('<input type="text" name="name" id="name" />');
      Add('<input type="submit" value="Send" />');
      Add('</form>');
    end
  else
    AResponse.Content := 'Ciao, ' + LName + '!';
  Handled := true;
end;
```

`ARequest.URI` è giusto una convenzione per fare riferimento all'URI attuale, in questo modo anche se cambiate il modulo registrato o il nome dell'azione, questo codice resta uguale.

Notare che in Pascal, il riferimento ad un dato è effettuato in modalità case insensitive.

Ora potete provare richiedendo /, che mostrerà

> Per favore dimmi il tuo nome

e /?name=&lt;scrivere qualcosa qui, es.: Bob&gt;, e poi verrà visualizzato

> Ciao, Bob!

## Lettura POST

POST in realtà non differisce molto da GET, l'unica differenza è nella properietà di accesso. Se a GET si accede tramite `ARequest.QueryFields`, a POST si accede tramite `ARequest.ContentFields`. 
Il codice precedente con stile POST è:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
var
  LName: String;
begin
  LName := ARequest.ContentFields.Values['Name'];
  if LName = EmptyStr then
    with AResponse.Contents do begin
      Add('<form action="' + ARequest.URI + '" method="POST"');
      Add('<label for="name">Per favore dimmi il tuo nome:</label>');
      Add('<input type="text" name="name" id="name" />');
      Add('<input type="submit" value="Send" />');
      Add('</form>');
    end
  else
    AResponse.Content := 'Ciao, ' + LName + '!';
  Handled := true;
end;
```

## Lettura File Upload

Una eccezione è nella lettura dei campi `multipart/form-data`, ad esempio i file. Questi sono disponibili in `ARequest.Files` come istanze di `TUploadedFiles`, che è un discendente di `TCollection`. Segue l'interfaccia pubblica di TUploadedFiles che potete usare per accedere ai file:

```pascal
TUploadedFiles = Class(TCollection)
...
public
  Function First : TUploadedFile;
  Function Last : TUploadedFile;
  Function IndexOfFile(AName : String) : Integer;
  Function FileByName(AName : String) : TUploadedFile;
  Function FindFile(AName : String) : TUploadedFile;
  Property Files[Index : Integer] : TUploadedFile read GetFile Write SetFile; default;
end;
```

Ogni `TUploadedFile` ha diverse proprietà:

```pascal
TUploadedFile = Class(TCollectionItem)
...
Public
  Destructor Destroy; override;
  Property FieldName : String Read FFieldName Write FFieldName;
  Property FileName : String Read FFileName Write FFileName;
  Property Stream : TStream Read GetStream;
  Property Size : Int64 Read FSize Write FSize;
  Property ContentType : String Read FContentType Write FContentType;
  Property Disposition : String Read FDisposition Write FDisposition;
  Property LocalFileName : String Read FLocalFileName Write FLocalFileName;
  Property Description : String Read FDescription Write FDescription;
end;
```

Dovrebbero essere abbastanza descrittive, con l'eccezione di `FileName` e `LocalFileName`.
`FileName` è il **nome** originale del file come caricato dal client, `LocalFileName`
è il **percorso** del file nel server dove il file viene temporaneamente memorizzato. Notare sopra le differenze dei termini in grassetto.

Ancora, riutilizzando lo stesso gestore di richiesta:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
var
  n: Integer;
  f: TUploadedFile;
  i: Integer;
begin
  n := ARequest.Files.Count;
  if n = 0 then
    with AResponse.Contents do begin
      Add('<form id="form" action="' + ARequest.URI + '" method="POST" enctype="multipart/form-data">');
      Add('<label for="name">Drag/drop o clicca per aggiungere un file:</label>');
      Add('<input type="file" name="input" />');
      Add('<input type="submit" value="Send" />');
      Add('</form>');
    end
  else begin
    f := ARequest.Files[0];
    AResponse.Contents.LoadFromStream(f.Stream);
  end;
  Handled := true;
end;
```

Fate il Drag/drop di un file (preferibilmente di testo, in quanto verrà renderizzato come testo) nel campo di input (o cliccate il rispettivo bottone) poi cliccate il bottone Send. Dovrebbe essere visualizzato il contenuto del file.

# Cookies

## Impostazioni

Il salvataggio e la manutenzione dei Cookies sono sotto la responsabilità del browser, per questo motivo quando bisogna imostarne uno, il server deve spedirlo come parte della risposta. `AResponse.Cookies` contiene una lista cookies da spedire. E' un discendente di `TCollection`, rispettivamente il `TCookie` contenuto è un discendente di `TCollectionItem`. Pertanto per gestire gli elementi potete operare come solitamente fate quando usate un  `TCollection`.

Ecco un esempio:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
var
  C: TCookie;
begin
  C := AResponse.Cookies.Add;
  C.Name := 'mycookie';
  C.Value := 'somevalue';
  Handled := true;
end;
```

Non si vedrà alcun output nel browser. Ma se userete un qualunque developer tools (Chrome ne ha uno
integrato), potrete vedere l'intestazione della risposta:

![Header della risposta set-cookie nel developer tool di Chrome](setcookie.png "Header della risposta set-cookie nel developer tool di Chrome")

Header della risposta set-cookie nel developer tool di Chrome



Notare che i Cookie hanno gli attributi, Name e Value non sono gli unici due che potete impostare. Esaminate l'interfaccia TCookie per vedere quali proprietà sono supportate.

## Lettura

Una volta data l'intestazione `Set-Cookie`, la successiva richiesta al sito conterrà l'intestazione aggiuntivo contenente il valore che si è chiesto di impostare in precedenza:

![Header della richiesta set-cookie nel developer tool di Chrome](getcookie.png "Header della richiesta set-cookie nel developer tool di Chrome")

Header della richiesta set-cookie nel developer tool di Chrome



Fortunatamente, la modalità di lettura non è diversa da quella GET e POST. La relativa proprietà è `ARequest.CookieFields`. Per leggere il coockie precedentemente impostato:

```pascal
procedure TFPWebModule1.DataModuleRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
begin
  AResponse.Contents.Add('<p>Cookie get: ' + ARequest.CookieFields.Values['mycookie'] + '</p>');
  Handled := true;
end;
```

# Sessioni

TFPWebModule è un discendente di TSessionHTTPModule, quindi ha capacità di gestione della sessione. La sessione è basata sul modulo, così ogni modulo può scegliere di utilizzare o non utilizzare la gestione delle sessioni.

La sessione è implementata in modo astratto. Come default, non viene fornita alcuna implementazione. Un esempio di implementazione che usa i file .ini è nella unit `iniwebsession`. Bisogna avere questa unità nel progetto o implementarne una per la gestione delle sessioni per funzionare. Se si decide di implementarne uno, fondamentalmente è necessario derivare ed implementare i metodi astratti nelle classi `TCustomSession` e `TSessionFactory`.

## Attivazione

Per attivare la gestione delle sessioni, impostare la proprietà `CreateSession` a true. La sessione inizierà prima della gestione della richiesta. In caso di nuova sessione, verrà chiamato l'evento `OnNewSession`. Quì si possono inizializzare le variabili di sessione.

## Gestione delle variabili di sessione

Le variabili di sessione sono fornite come `Session.Variables`. Questa è una mappa con struttura tipo stringa a stringa, in modo da poter leggere / scrivere come:

```pascal
Session.Variables['miavariabile'] := miavariabile; // scrittura
...
miavariabile := Session.Variables['miavariabile']; // lettura
```

Impostare una variabile a stringa vuota **non** la rimuove. Se si vuole rimuovere una variabile veramente, chiamare invece `Session.RemoveVariable`.

## Terminare

Chiamare `Session.Terminate` ogni volta che si desidera terminare una sessione (ad esempio: logout utente). La sessione scadrà automaticamente se la richiesta successiva avviene dopo `Session.TimeOutMinutes` dall'ultima richiesta. Quando la sessione termina, viene chiamato `OnSessionExpired`. Fate quì la pulizia di ciò che deve essere liberato.

# Utilizzo di più moduli

Si possono avere più moduli in una applicazione. Fare clic sul menu "File", poi cliccare su "Nuovo ...".
Apparirà una finestra di dialogo, selezionare "Web Module" dalla vista ad albero.

![Aggiunta modulo web](newwebmodule.png "Aggiunta modulo web")

Aggiunta modulo web



poi cliccare OK.

Quando in una applicazione esistono più moduli web, non è possibile effettuare la richiesta con solo /. Il framework non sarà in grado di selezionare magicamente quale modulo dovrà servire la risposta, quindi ci sono due modi per stabilire quale modulo si desidera chiamare:

1. /&lt;nome del modulo&gt;
2. /?module=&lt;nome del modulo&gt;

Nel 2° formato, è possibile sostituire "modulo" (che è il valore di default) con qualsiasi altra stringa di query valida modificando `Application.ModuleVariable`.

# Utilizzo delle azioni

Finora, abbiamo utilizzato solo moduli web con singolo gestore di richieste. Questo non **scalerà** tanto quanto la vostra applicazione web diventa sempre più complessa. Inoltre, alcune funzioni potrebbero avere delle proprietà condivise ed essere convenientemente raggruppate logicamente, ad esempio:

* Modulo Autenticazione
    * Azione Login
    * Azione Logout
    * Azione Registrazione

* Modulo Articolo
    * Azione Crea
    * Azione Aggiorna
    * Azione Elimina
    * Azione Dettaglio

## Flusso di gestione della richiesta

Prima di utilizzare un'azione, è importante conoscere il flusso di gestione della richiesta di fpWeb. In caso contrario l'azione potrebbe risultare inutile in quanto è sempre il modulo dati che gestisce la richiesta. Come può essere? Tornando indietro di qualche capitolo, ricordate il `Handled := true` che abbiamo sempre fatto prima? Questo è il momento in cui il parametro Handled entra in gioco.

**Tutte** le richieste passeranno prima per il OnRequest del modulo, a prescindere l'azione richiesta. Solo se **non** viene impostato `Handled` a true, viene eseguito l'evento OnRequest dell'azione web.

In generale, il flusso di richiesta è:

![Flusso richiesta di fpWeb](fpwebrequestflow.png "Flusso richiesta di fpWeb")

Flusso richiesta di fpWeb



Notate il blocco "Our Concern" (di Nostro Interesse), che è quello su cui andrà a posarsi la nostra attenzione.

## Aggiungere Azioni ai Moduli Web

Per aggiungere un'azione, selezionare il modulo web ed andare all'object innspector. Nella scheda delle proprietà, selezionare `Azioni` e fare clic sul bottone sulla seconda colonna.

![Pulsante gestione azioni nell'object inspector](manageactionsobjectinspector.png "Pulsante gestione azioni nell'object inspector")

Pulsante gestione azioni nell'object inspector



Apparirà una finestra popup dove sarà possibile aggiungere, cancellare e riordinare le azioni.

![Pulsante gestione azioni nella finestra popup](manageactionspopupwindow.png "Pulsante gestione azioni nella finestra popup")

Pulsante gestione azioni nella finestra popup



Premere Aggiungi, una nuova azione comparirà nell'elenco. Selezionarla e poi andare nell'object inspector. Sarà visualizzare attualmente le proprietà e gli eventi di questa azione appena creata. Rinominare la proprietà `Name` (questo sarà il nome da utilizzare nella URL, dare un nome breve, semplice ma descrittivo) come volete, io sceglierò "Hello". 
Passare al tab eventi, fare lo stesso come per l'evento OnRequest del Modulo, cliccare il bottone sulla destra della riga dell'evento OnRequest per creare il gestore della richiesta.

![Creazione gestore della richiesta web nell'OnRequest dell'azione](webactiononrequestobjectinspector.png "Creazione gestore della richiesta web nell'OnRequest dell'azione")

Creazione gestore della richiesta web nell'OnRequest dell'azione



Vi troverete nella stessa interfaccia OnRequest, ma questa gestisce l'azione web invece del modulo web. Quello che si può fare nell'evento OnRequest del modulo web può essere fatto anche qui. Copiare il corpo del metodo della sezione "Ciao, mondo!".

Ricordarsi di rimuovere `Handled := true` dal corpo dell'OnRequest precedente del modulo web (o rimuovere completamente l'evento) in modo che l'azione possa gestire la richiesta.

Eseguire il progetto e avviare il browser. Ora, dal momento che la gestione delle richieste è delegata all'azione web, non è più possibile richiedere semplicemente /, ma è necessario /&lt;nome azione&gt; o &lt;proprietà ActionVar del modulo&gt;=&lt;nome azione&gt;. Notare che la &lt;proprietà ActionVar del modulo&gt; ha come valore predefinito una stringa vuota, a differenza di Application.ModuleVariable che come valore predefinito ha "modulo". Quindi, come default, è possibile utilizzare solamente la forma /&lt;nome azione&gt;

Si se hanno più moduli, abbiamo diverse possibilità:

1. /&lt;nome modulo&gt;/&lt;nome azione&gt;
2. /&lt;nome modulo&gt;?action=&lt;nome azione&gt;
3. /&lt;nome azione&gt;?module=&lt;nome modulo&gt;
4. /?module=&lt;nome modulo&gt;&amp;action=&lt;nome azione&gt;

Notare che non appena un modulo ha almeno una azione, /&lt;nome modulo o azione&gt; da solo, verrà mappato come default come /&lt;action nome&gt;. Per modificare questo comportamento in modo venga mappato per default come /&lt;nome modulo&gt;, impostare Application.PreferModuleName a TRUE. In caso di più moduli, quando il nome del modulo non è dato, allora sarà il modulo predefinito a gestire l'azione richiesta. Per cambiare questo comportamento in modo che il nome del modulo debba essere esplicitamente indicato, impostare `Application.AllowDefaultModule` a `false`.

Le tabelle seguenti riassumono ciò che accadrà in base alle due proprietà:

<table border="1">
  <tr>
    <th colspan="2" rowspan="2">/&lt;module or action name&gt;</th>
    <th colspan="2">Application.PreferModuleName</th>
  </tr>
  <tr>
    <td>true</td>
    <td>false</td>
  </tr>
  <tr>
    <th rowspan="2">Application.AllowDefaultModule</th>
    <td>true</td>
    <td>/&lt;module name&gt;</td>
    <td>/&lt;default module&gt;/&lt;action name&gt;</td>
  </tr>
  <tr>
    <td>false</td>
    <td>/&lt;module name&gt;</td>
    <td>ERROR</td>
  </tr>
</table>

## Azione di Default

Riprendendo lo schema precedente, il "delegare gestione della richiesta alle azioni" in realtà non è così semplice ma, se espandiamo il diagramma, l'immagine finale sarebbe troppo grande. Quindi, ecco lo schema di quella parte:

![Flusso richiesta delegata all'azione](delegaterequesthandlingtoactions.png "Flusso richiesta delegata all'azione")

Flusso richiesta delegata all'azione



Due cose importanti dal flusso: DefActionWhenUnknown e azione di default. Il primo è una proprietà del modulo web mentre l'altro alla proprietà Default di un'azione. In quest'ultimo caso, nel caso vi siano più di due azioni con la proprietà `Default` impostata a true, sarà preso in considerazione l'ordine dell'azione (così come mostrato nella finestra popup di gestione delle azioni)  per decidere quale sia l'azione predefinita. Le due proprietà determinano ciò che l'applicazione deve fare se non viene trovata nessuna azione per soddisfare una data richiesta.

Le tabelle seguenti riassumono ciò che accadrà in base alle due proprietà:

<table border="1">
  <tr>
    <th colspan="2" rowspan="2" align="center" valign="center">Request with<br />invalid action name</th>
    <th colspan="2" align="center" valign="center">DefActionWhenUnknown</th>
  </tr>
  <tr>
    <td>true</td>
    <td>false</td>
  </tr>
  <tr>
    <th rowspan="2" align="center" valign="center">Action.Default</th>
    <td>true</td>
    <td>Request handled by<br />default action</td>
    <td>Error: No action found for<br />action: &lt;action name&gt;</td>
  </tr>
  <tr>
    <td>false</td>
    <td>Error: Invalid action name<br />and no default action</td>
    <td>Error: No action found for<br />action: &lt;action name&gt;</td>
  </tr>
</table>



<table border="1">
  <tr>
    <th colspan="2" rowspan="2" align="center" valign="center">Request without<br />action name, i.e.: /</th>
    <th colspan="2" align="center" valign="center">DefActionWhenUnknown</th>
  </tr>
  <tr>
    <td>true</td>
    <td>false</td>
  </tr>
  <tr>
    <th rowspan="2" align="center" valign="center">Action.Default</th>
    <td>true</td>
    <td>Request handled by<br />default action</td>
    <td>Request handled by<br />default action</td>
  </tr>
  <tr>
    <td>false</td>
    <td>Error: No action name and no default action</td>
    <td>Error: No action name and no default action</td>
  </tr>
</table>

Nel caso di risposta errata come sopra indicato, deve seguire uno stacktrace, completo di informazioni sul numero di riga se avete compilato l'applicazione con -gl. Vedremo più avanti come creare un gestore personalizzato per questi casi (gli stacktrace non vanno bene in produzione). Ma per ora, bisogna assicurarsi di aver capito il concetto di modulo web e di azione web, ed in particolare il flusso di richiesta. Sperimentate fino a quando penserete di essere pronti per la prossima sezione.

# Utilizzo dei Template

fpWeb il supporto integrato per FPTemplate, il motore generico di template di Free Pascal. Non deve essere utilizzato nel contesto di una applicazione web, ma con il supporto intergato di questa cosa sarà un po' più facile. Quantomeno possiamo ignorare la gestione della memoria in quanto se ne occuperà il modulo.

Ci sono due livelli in cui si possono utilizzare i template: azione e il modulo. Il supporto RAD non è completo, quindi sarà necessario scrivere codice a mano in alcuni punti.

Ci sono due modalità di funzionamento: non parametrizzato e parametrizzato. La modalità attiva è controllata dalla proprietà AllowTagParams, e dovrebbe essere abbastanza evidente quale valore si riferisce a quale modalità.

La stringa template può essere data da un file tramite la proprietà FileName o come stringa direttamente attraverso la proprietà Template.  
Template.Template, so che suona strano :) Se entrambi sono valorizzati allora NomeFile avrà la precedenza.

Le due proprietà: `StartDelimiter` e `EndDelimiter` definiscono come il motore dovrebbe riconoscere un tag del template. Per esempio, abbiamo:

* StartDelimiter = '{+'
* EndDelimiter   = '+}'

allora la stringa '{+title+}' definisce un tag del template di nome 'title'. Notare che gli spazi sono significativi, quindi '{+ title +}' definisce un tag ' titolo ' invece del semplice 'titolo'.

Relativamente alla modalità parametrizzata, tre ulteriori proprietà: `ParamStartDelimiter`,
`ParamEndDelimiter` e `ParamValueSeparator` definiscono come il motore dovrebbe riconoscere un parametro di un tag del template. Ad esempio, se abbiamo:

* ParamStartDelimiter = '[-'
* ParamEndDelimiter   = '-]'
* ParamValueSeparator = '='

allora la stringa '{+data [-p=v-] [-a=b-] +}' definisce un tag 'data'
con parametro 'p' di valore 'v' e il parametro 'a' di valore 'b'. Questo può essere usato per passare parametri a livello di template come il formato della data prevista, header-­row-­footer per la personalizzazione dell'output, nome di un file, eccetera; decidete voi.

Come conseguenza del diverso modo di funzionamento, anche l'evento centrale, tramite il quale lavora il modello, è diverso. Nella modalità non-parametrizzata verrà usato l'evento `OnGetParam` mentre nel modo parametrizzato verrà usato OnReplaceTag. I due eventi hanno ovviamente interfacce diverse:

```pascal
Type
  // OnGetParam: for simple template tag support only (ex: {Name})
  TGetParamEvent = Procedure(
    Sender : TObject;
    Const ParamName : String;
    Out AValue : String
  ) Of Object;
  // OnReplaceTag: for tags with parameters support
  TReplaceTagEvent = Procedure(
    Sender : TObject;
    Const TagString : String;
    TagParams:TStringList;
    Out ReplaceText : String
  ) Of Object;
```

In `OnGetParam`, si controlla `ParamName`, quindi si imposta di conseguenza `AValue`. Ad esempio, se si vuoleche il tag 'title' venga sostituito sostituito con 'My App', si può codificare nel metodo qualcosa come segue:

```pascal
// use Trim() if you want the spaces around tag to be insignificant
case Trim(ParamName) of
  'title': AValue := 'My App';
  else     AValue := 'UNKNOWN';
end;
```

In `OnReplaceTag`, si controlla`TagString` ed eventualmente `TagParams`, di conseguenza si imposta `ReplaceText`. Ad esempio se si vuole sostituire il tag 'datetime' con l'ora corrente con il parametro 'datetimeformat' per specificare come l'ora deve essere formattata, allora si può compilare il corpo del motodo come segue:

```pascal
// use Trim() if you want the spaces around tag to be insignificant
case Trim(TagString) of
  'datetime': AValue := FormatDateTime(TagParams.Values['datetimeformat'],Now);
  else        AValue := 'UNKNOWN';
end;
```

## A livello di Azione

Create/selezionate un'azione, poi andare nell'object inspector. Vedrete una proprietà sottocomponente chiamata `Template`. Questo template è una normale istanza di `TFPTemplate`. Espandetela e compilate le proprietà come spiegato sopra. Andate ora nel tabe Eventi, di nuovo espandete `Template`, vedrete i due eventi. Scrivete in quello basato sul vostro valore della proprietà `AllowTagParams`.

NOTA:
Se il vostro Lazarus non può completare automaticamente l'evento, provare a scrivere il nome manualmente nella casella di modifica e fare clic sul bottone con ... Questo è un bug presente in Lazarus che può essere corretto in futuro.

A questo livello, un template con un contenuto non viene automaticamente impostato come destore dalla richiesta. Si potrebbe cambiare in futuro, ma cerchiamo di affrontare la situazione corrente. Generate l'evento
`OnRequest` dell'azione, poi compilatelo come:

```pascal
with Actions.CurrentAction as TFPWebAction do begin
  AResponse.Content := Template.GetContent;
end;
Handled := true;
```

Il cast è necessario in quanto `CurrentAction` è di tipo `TCustomWebAction` invece che `TFPWebAction`. Senza questo, non possiamo accedere alla proprietà `Template`.

## A livello di Modulo

A livello di modulo, attualmente bisogna scrivere codice a mano in quanto non è stato implementato alcun supporto RAD. La proprietà interessata è  `ModuleTemplate`. Questa comunque non è una normale istanza di `TFPTemplate`, ma un classe speciale `TFPWebTemplate` che ne è discendente.

L'idea è quella di avere modulo che fornisce il layout mentre le azioni forniscono i contenuti, con la possibilità di fornire variabili aggiuntive. E' quindi una buona idea lasciare `AllowTagParams` così com'è e assegnare solamente `OnGetParam` **del modulo**.
NON ASSEGNARE `OnGetParam` del `ModuleTemplate` in quanto non verrà **mai** chiamato.

Il tag del template chiamato 'content' sarà automaticamente sostituito dal contenuto prodotto dall' azione, tutto il resto è sostituito dalle variabili del template interni o da `OnGetParam`.

## Uso di template separati

Nonostante l'integrazione un po' incompleta, nulla impedisce di utilizzare fpTemplate (o altre soluzioni di template) manualmente, fuori dal supporto integrato. Questo potrebbe anche essere meglio in alcuni casi in quanto è modulare.

# Tips and Tricks

## Centralizzare gestione della configurazione e Moduli

Come default, il file di programma (.lpr) è quello che contiene la unit del protocollo. Questo limita la capacità di utilizzare l'oggetto `Application` da altri contesti, ad esempio dai moduli web. Fortunatamente, non è difficile fare un refactoring per ottevere ciò che ci interessa.
Togliamo le chiamate a `RegisterHTTPModule` dalle unit dei moduli web e le lasciamo fuori dal .lpr per svuotare blocco principale e lasciarlo con un unico identificatore di una singola unit nella clausola uses, la chiameremo: broker. La unit contiene:

```pascal
unit Brokers;

{$mode objfpc}{$H+}

interface

{ $define cgi}
{ $define fcgi}
{$define httpapp}

uses
  CustWeb;

function GetApp: TCustomWebApplication; inline;

implementation

uses
  {$ifdef cgi}
  fpcgi
  {$endif}
  {$ifdef fcgi}
  fpfcgi
  {$endif}
  {$ifdef httpapp}
  fphttpapp
  {$endif}
  ,webmodule1
  ,webmodule2
  ;

function GetApp: TCustomWebApplication;
begin
  Result := Application;
end;

initialization
  RegisterHTTPModule('wm1', TWebModule1);
  RegisterHTTPModule('wm2', TWebModule2);
  {$ifndef cgi}
  Application.Port := 2015;
  {$endif}
  Application.Initialize;
  Application.Run;

end.
```

In questo modo, siamo in grado di controllare la registrazione del modulo web e fornire anche una API per ottenere l'oggetto `Application` (tramite cast a TCustomWebApplication), mentre possiamo ancora commutare facilmente tra le implementazioni del protocollo, in un unico luogo.

## Terminazione ottimale (FastCGI / Embedded Web Server)

Invece di CTRL+C-are la vostra applicazione, c'è un modo ottimale per terminarla, facendo tutta la pulizia di cui c'è bisogno, chiamando Application.Terminate. Potrebbe essere necessario utilizzare trucco precedente per accedere facilmente all'oggetto Application. Una possibile implementazione è quella di prevedere un modulo/azione protetto da una specifica password che chiama il metodo Terminate. Potreste però scegliere qualsiasi altro modo desiderato.

## Gestione personalizzata delle eccezioni

Per fare l'override del gestore di eccezioni di default, che stampa uno stacktrace ogni volta che viene sollevata un'eccezione (es: su HTTP 404 o 500), e che quindi non va bene per la produzione, è necessario assegnare `Application.OnShowRequestException`. Si tratta di un metodo e quindi è necessario prevedere un oggetto che implementi il metodo per poi assegnarlo. Es: se avete `MyExceptionHandler` come istanza di `TMyExceptionHandler`
che ha il metodo `MyShowRequestException`, è possibile assegnarlo con:

```pascal
Application.OnShowRequestException := @MyExceptionHandler.MyShowRequestException;
```

non dimenticate di chiamare `MyExceptionHandler.Create` PRIMA dell'assegnamento sopra o si otterrà un `EAccessViolation`!

## Codifica manuale pura (non richiede Form Designer)

Non è un must usare il form designer di Lazarus per scrivere un'applicazione fpWeb. Si può usare una tecnica di pura codifica manuale per scriverla. Il segreto è nel 3 ° parametro di `RegisterHTTPModule`: `SkipStreaming`. Quando questo parametro è impostato su true, fpWeb non cercherà le risorse .lfm. Quindi tutto deve essere gestito manualmente: impostazioni di proprietà, gestori di eventi, registrazione di azioni, ecc.

`TFPWebModule` discende da `TDataModule`, così potete usare l'evento `OnCreate` per inizializzare tutto ciò di cui il modulo necessita. Sentitevi liberi di dare uno sguardo a ciò forniscono la classe ed i suoi antenati per fare tutto quello che di solito si fa con il form designer.
