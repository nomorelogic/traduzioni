Repository per traduzioni di documentazione tecnica Free Pascal

# Attenzione

Non sono sicuramente un traduttore di madrelingua inglese ma programmo in object pascal da
diversi anni (prima Turbo Pascal, poi Delphi ed da un po' di tempo anche Free Pascal). 
In questo repository si trovano testi tecnici, riguardanti principalmente
Free Pascal e Lazarus, che hanno attirato la mia attenzione e, visto che la documentazione
in italiano scarseggia sempre, spero che questo mio sforzo possa aiutare qualcuno ad avvicinare
queste tecnologie.


# fpWeb Tutorial di base
E' attualmente in fase di traduzione, posto in questo repository durante i lavori, abbiate pazienza che entro qualche 
giorno sarà terminato ;)

Il documento originale lo trovate in 
https://bitbucket.org/leledumbo/books/src/99c3345949801c35766454cbd32d7bae3bb308c3/fpwebtutorial/?at=default
